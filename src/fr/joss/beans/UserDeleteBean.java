package fr.joss.beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;

import fr.joss.classes.User;
import fr.joss.dao.UserDAO;
import fr.joss.entities.UserEntity;


@ManagedBean
@RequestScoped
public class UserDeleteBean {
	User user = new User();

	public UserDeleteBean() {
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}
	
	public void findUser() {
		user = UserDAO.findUser(user.getIdUser());
	}
	
	
public String deleteUser() {
		
		
		UserDAO.deleteUser(user);
		
		
		return "ajoutUser.xhtml?faces-redirect=true";
	}
	
	
}
