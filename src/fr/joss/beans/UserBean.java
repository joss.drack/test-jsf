package fr.joss.beans;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import fr.joss.classes.User;
import fr.joss.dao.UserDAO;
import fr.joss.entities.UserEntity;

@ManagedBean
@SessionScoped
public class UserBean {

	private int idUser;
    private String nom;
    private String prenom;
    private String login;
    private String password;
    private String email;
    private boolean profil;
    private List<User> lst ;
    private User user;
  
    
    /**
     * Creates a new instance of UserBean
     */
    public UserBean() {
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

	public int getIdUser() {
		return idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public boolean isProfil() {
		return profil;
	}

	public void setProfil(boolean profil) {
		this.profil = profil;
	}

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    /**
	 * @return the lst
	 */
	public List<User> getLst() {
		return lst;
	}

	/**
	 * @param lst the lst to set
	 */
	public void setLst(List<User> lst) {
		this.lst = lst;
	}
	
	public void initList(){
	    this.lst = UserDAO.listUsers();
	}
	
	public String addUser(){
    	User u = new User(nom, prenom, login, password, email);
        UserDAO.addUser(u);
        return "ajoutUser.xhtml?faces-redirect=true";
    }

   
}
