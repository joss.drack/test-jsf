package fr.joss.beans;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import fr.joss.classes.User;
import fr.joss.dao.UserDAO;
import fr.joss.entities.UserEntity;

@ManagedBean
@RequestScoped()
public class UserUpdateBean {
	
	User user = new User();
	
    /**
     * Creates a new instance of UserBean
     */
    public UserUpdateBean() {
    }

    
	
	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}



	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	public void findUser() {
		user = UserDAO.findUser(user.getIdUser());
	}

	public String updUser() {
		
		UserDAO.updateUser(user);
		
		return "ajoutUser.xhtml?faces-redirect=true";
	}
}
