package fr.joss.beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import fr.joss.classes.User;
import fr.joss.dao.UserDAO;

@ManagedBean
@SessionScoped
public class LoginBean {

	private String email;
	private String password;
	private boolean cnx;
	
	public LoginBean() {}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the cnx
	 */
	public boolean isConnexion() {
		return cnx;
	}

	/**
	 * @param connexion the cnx to set
	 */
	public void setConnexion(boolean connexion) {
		this.cnx = connexion;
	}
	
	public String connexion() {
		
		//User u = new User();
		//u.setEmail(email);
		//u.setPassword(password);
		//u.setPasswordAsh(password);
		
		if (UserDAO.findByLogin(password,email)) {
			this.cnx = true;
			return "home";
		}
			
		
		return "erreur";
	}
	
	
	
	
}
