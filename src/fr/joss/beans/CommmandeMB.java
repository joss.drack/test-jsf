package fr.joss.beans;

import fr.joss.classes.Commande;
import fr.joss.classes.User;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@ManagedBean
@RequestScoped
public class CommmandeMB {


	private Commande cmd = new Commande();
	private List<User> lstClts = new ArrayList<User>();
	private List<Commande> lstCommandes = new ArrayList<Commande>();
	private String numero;
	private Date dateCmd;
	private String remise;
	private User user;

	
	public CommmandeMB() {}


	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Date getDateCmd() {
		return dateCmd;
	}

	public void setDateCmd(Date dateCmd) {
		this.dateCmd = dateCmd;
	}

	public String getRemise() {
		return remise;
	}

	public void setRemise(String remise) {
		this.remise = remise;
	}
/*
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
*/

	public void initLst() {
		lstClts = new ArrayList<>();//UserDAO.listUsers();
		lstCommandes = new ArrayList<>();// CommandeDao.listCommandes();
	}

	public List<User> getLstClts() {
		return lstClts;
	}

	public void setLstClts(List<User> lstClts) {
		this.lstClts = lstClts;
	}

	public List<Commande> getLstCommandes() {
		return lstCommandes;
	}

	public void setLstCommandes(List<Commande> lstCommandes) {
		this.lstCommandes = lstCommandes;
	}

	public void createNumeroCommande() {
		
		Date d = new Date();
		String num = new SimpleDateFormat("yyMMdd").format(d);
		
		String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
	    int charLength = chars.length();
	    
	        StringBuilder  pass = new StringBuilder (charLength);
	        for (int x = 0; x < 4; x++) {
	            int i = (int) (Math.random() * charLength);
	            pass.append(chars.charAt(i));
	        }
	        
		cmd.setNumero(num+pass.toString());
		cmd.setRemise("0.0");
		
	}
	
	
	 public String ajouterCmd() {

			System.out.println("--------DEBUT-----");
			cmd.setNumero(numero);
			cmd.setRemise(remise);
			cmd.setDateCmd(dateCmd);
			//cmd.setUser(user);
			//CommandeDao.ajout(cmd);
			lstCommandes.add(cmd);
		 	System.out.println("--" + "------FIN------------");
			return "ajoutCmd";
	}
	


}
