package fr.joss.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Connexion {
	//@PersistenceContext(unitName="joss")
		protected static EntityManagerFactory emf;
		//@PersistenceUnit(unitName="joss")
		protected static EntityManager em;

		public static EntityManager connexion() {
			
	
				
				emf = Persistence.createEntityManagerFactory("SONIA_PROJET");
				em = emf.createEntityManager();
				System.out.println("CONNEXION OK===========>>>>>");
				

			return em;
		}

		public static void deconnexion() {
			if(em != null) 
				em.close();
			
			if(emf != null)
				emf.close();
		}
}
