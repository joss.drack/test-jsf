package fr.joss.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Entity implementation class for Entity: CommandeEntity
 *
 */
@Entity(name="commande")
@Table(name="commande")
public class CommandeEntity implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_cmd")
	private int idCmd;
	private String numero;
	@Temporal(TemporalType.DATE )
	@Column(name = "date_cmd")
	private Date dateCmd;
	private String remise;
	@ManyToOne
	private UserEntity user;

	public CommandeEntity() {
		super();
	}

	/**
	 * @return the iCmd
	 */
	public int getIdCmd() {
		return idCmd;
	}

	/**
	 * @param iCmd the iCmd to set
	 */
	public void setIdCmd(int iCmd) {
		this.idCmd = iCmd;
	}

	/**
	 * @return the numero
	 */
	public String getNumero() {
		return numero;
	}

	/**
	 * @param numero the numero to set
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}

	/**
	 * @return the dateCmd
	 */
	public Date getDateCmd() {
		return dateCmd;
	}

	/**
	 * @param dateCmd the dateCmd to set
	 */
	public void setDateCmd(Date dateCmd) {
		this.dateCmd = dateCmd;
	}

	/**
	 * @return the remise
	 */
	public String getRemise() {
		return remise;
	}

	/**
	 * @param remise the remise to set
	 */
	public void setRemise(String remise) {
		this.remise = remise;
	}

	/**
	 * @return the user
	 */
	public UserEntity getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(UserEntity user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "CommandeEntity [iCmd=" + idCmd + ", numero=" + numero + ", dateCmd=" + dateCmd + ", remise=" + remise
				+ ", user=" + user + "]";
	}
	
	 
   
}
