package fr.joss.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Entity implementation class for Entity: UserEntity
 *
 */
@Entity(name="user")
@Table(name ="user")
public class UserEntity implements Serializable {




	/**
	 * 
	 */
	private static final long serialVersionUID = -6976348984795181781L;
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_user")
    private Integer idUser;
    private String nom;
    private String prenom;
    @Basic(optional = false)
    private String login;
    @Basic(optional = false)
    private String password;
    @Basic(optional = false)
    private String email;
    @Basic(optional = false)
    private boolean profil;
    @OneToMany (mappedBy = "user")
    private List<CommandeEntity> lstCmd;

    public UserEntity() {
    }

    public UserEntity(Integer idUser) {
        this.idUser = idUser;
    }
    
   

    public UserEntity(String nom, String prenom, String login, String password, String email) {
		this.nom = nom;
		this.prenom = prenom;
		this.login = login;
		this.password = password;
		this.email = email;
	}

	

    public UserEntity(String nom, String prenom, String login, String password, String email, boolean profil) {
		this.nom = nom;
		this.prenom = prenom;
		this.login = login;
		this.password = password;
		this.email = email;
		this.profil = profil;
	}
    
    

	public UserEntity(Integer idUser, String nom, String prenom, String login, String password, String email,
			boolean profil) {
		this.idUser = idUser;
		this.nom = nom;
		this.prenom = prenom;
		this.login = login;
		this.password = password;
		this.email = email;
		this.profil = profil;
	}
	
	

	public UserEntity(Integer idUser, String nom, String prenom, String login, String password, String email) {
		this.idUser = idUser;
		this.nom = nom;
		this.prenom = prenom;
		this.login = login;
		this.password = password;
		this.email = email;
	}

	public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean getProfil() {
        return profil;
    }

    public void setProfil(boolean profil) {
        this.profil = profil;
    }
    
    

    /**
	 * @return the lstCmd
	 */
	public List<CommandeEntity> getLstCmd() {
		return lstCmd;
	}

	/**
	 * @param lstCmd the lstCmd to set
	 */
	public void setLstCmd(List<CommandeEntity> lstCmd) {
		this.lstCmd = lstCmd;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (idUser != null ? idUser.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserEntity)) {
            return false;
        }
        UserEntity other = (UserEntity) object;
        if ((this.idUser == null && other.idUser != null) || (this.idUser != null && !this.idUser.equals(other.idUser))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "idUser=" + idUser;
    }
    
}
