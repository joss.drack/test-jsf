package fr.joss.classes;

import org.mindrot.jbcrypt.BCrypt;

public class User {
	private Integer idUser;
    private String nom;
    private String prenom;
    private String login;
    private String password;
    private String email;
    private boolean profil;
    //private String passwordAsh;

    public User() {}
    
    
    
    public User(Integer idUser) {
		this.idUser = idUser;
	}



	public User(String nom, String prenom, String login, String password, String email) {
        this.nom = nom;
        this.prenom = prenom;
        this.login = login;
        this.password = password;
        this.email = email;
    }

    public User(String nom, String prenom, String login, String password, String email, boolean profil) {
        this.nom = nom;
        this.prenom = prenom;
        this.login = login;
        this.password = password;
        this.email = email;
        this.profil = profil;
    }
    
    

    public User(Integer idUser, String nom, String prenom, String login, String password, String email,
			boolean profil) {
		this.idUser = idUser;
		this.nom = nom;
		this.prenom = prenom;
		this.login = login;
		this.password = password;
		this.email = email;
		this.profil = profil;
	}
    
    

	public User(Integer idUser, String nom, String prenom, String login, String password, String email) {
		this.idUser = idUser;
		this.nom = nom;
		this.prenom = prenom;
		this.login = login;
		this.password = password;
		this.email = email;
	}

	public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {

        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isProfil() {
        return profil;
    }

    public void setProfil(boolean profil) {
        this.profil = profil;
    }
  
    
    /**
	 * @return the passwordAsh
	 */
    /*
	public String getPasswordAsh() {
		return passwordAsh;
	}

	/**
	 * @param passwordAsh the passwordAsh to set
	 */
    /*
	public void setPasswordAsh(String passwordAsh) {
		
		// Hachage d'un mot de passe pour la première fois
    	//String hashed = BCrypt.hashpw(password, BCrypt.gensalt());//
    	// Le paramètre log_rounds de gensalt détermine la complexité
    	// le facteur de travail est 2 ** log_rounds, et la valeur par défaut est 10
    	
		String hashed = BCrypt.hashpw(password, BCrypt.gensalt(23));
		this.passwordAsh = hashed;
	}
*/
	public boolean testPassword(String chaineATester, String mdp) {
    	// Vérifier qu'un mot de passe non chiffré correspond à celui qui a
    	// précédemment haché
    	
    	System.out.println(chaineATester);
    	System.out.println(mdp);
    	
    	if (BCrypt.checkpw(chaineATester, mdp))
    		return true;
    	else
    		return false;
    }

    @Override
    public String toString() {
        return "User{" + "nom=" + nom + ", prenom=" + prenom + ", login=" + login + ", password=" + password + ", email=" + email + ", profil=" + profil + '}';
    }
    
}
