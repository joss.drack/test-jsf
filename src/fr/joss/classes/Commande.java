package fr.joss.classes;


import java.util.Date;

public class Commande {

    private int idCmd;
    private String numero;
    private Date dateCmd;
    private String remise;
    private User user;

    public Commande() {
    }

    public Commande(String numero, Date dateCmd, String remise, User user) {
        this.numero = numero;
        this.dateCmd = dateCmd;
        this.remise = remise;
        this.user = user;
    }

    public int getIdCmd() {
        return idCmd;
    }

    public void setIdCmd(int idCmd) {
        this.idCmd = idCmd;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Date getDateCmd() {
        return dateCmd;
    }

    public void setDateCmd(Date dateCmd) {
        this.dateCmd = dateCmd;
    }

    public String getRemise() {
        return remise;
    }

    public void setRemise(String remise) {
        this.remise = remise;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Commande{" +
                "iCmd=" + idCmd +
                ", numero='" + numero + '\'' +
                ", dateCmd=" + dateCmd +
                ", remise='" + remise + '\'' +
                ", user=" + user +
                '}';
    }
}
