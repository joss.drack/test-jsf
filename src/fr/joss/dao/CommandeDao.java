package fr.joss.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import fr.joss.classes.Commande;
import fr.joss.util.Connexion;

public class CommandeDao {
	
	
	 public static void ajout(Commande c){
		 
	        EntityManager em = Connexion.connexion();

	        em.getTransaction().begin();
	       
	        em.persist(c);
	        
	        em.getTransaction().commit();
	        //Connexion.deconnexion();
	       
	    }
	    
	   
	    
	    public static List<Commande> listCommandes(){
	    	
	    	EntityManager em = Connexion.connexion();
	    	
	    	
	    	em.getTransaction().begin();
	    	
	    	
	    	TypedQuery<Commande> requete = em.createQuery("select c from commande c", Commande.class);
	    	
	    	List<Commande> commande  = requete.getResultList();
	    	
	    	 em.getTransaction().commit();

	         return commande;
	    }
	    
	    
	    
	public static Commande findCmd(int id){
	    	
	    	EntityManager em = Connexion.connexion();
	    	
	    	
	    	em.getTransaction().begin();
	    	
	    	
	    	Commande cmd = em.find(Commande.class, id);
		
	    	 em.getTransaction().commit();
	    	
	         return cmd;
	    }
	    
	    
	    
	    public static void deleteCmd(Commande c) {
	    	EntityManager em = Connexion.connexion();
	    	
	    	
	    	//Transaction 1 je crée mon objet
	    	em.getTransaction().begin();
	    	
	    	Commande cmd = em.find(Commande.class,c.getIdCmd());
	    	em.remove(cmd);

	    	em.getTransaction().commit();
	    	 
	    	System.out.println("remove--------------------");

	    }

}
