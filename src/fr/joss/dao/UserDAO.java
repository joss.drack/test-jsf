package fr.joss.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import fr.joss.classes.User;
import fr.joss.entities.UserEntity;
import fr.joss.util.Connexion;

public class UserDAO {
	
	public static boolean findByLogin(String password, String email) {
		System.out.println("findByLogin OK");
                    System.err.println(password + "  ======== " +email);
		EntityManager em = Connexion.connexion();
	
		
		em.getTransaction().begin();
		
		System.out.println("connecxion OK");
                    
                    
		Query requete = em.createNativeQuery("select u.id_user, u.nom, u.prenom,u.login, u.password, u.email "
									+ "from user u "
									+ "where password =:password  "
									+ "and email =:email");
		requete.setParameter("password",password);
		requete.setParameter("email",email);
		
		Object[] users = (Object[]) requete.getSingleResult();

		
		
		
		
		 boolean cnx = users != null ?  true :  false;
		        
		//System.out.println("1111111" + cnx);
		em.getTransaction().commit();
		
		//Connexion.deconnexion();
		
		return cnx;
	}
    
    public static void addUser(User u){
    	
        EntityManager em = Connexion.connexion();
      
        UserEntity userEntity = new UserEntity(u.getNom(), u.getPrenom(), u.getLogin(), u.getPassword(), u.getEmail());
     
        em.getTransaction().begin();
        em.persist(userEntity);
      
        em.getTransaction().commit();
		System.out.println("user persist");
        //Connexion.deconnexion();
       
    }
    
    public static void updateUser(User u){
    	
        EntityManager em = Connexion.connexion();
      
       //Debut de la transaction
        em.getTransaction().begin();
        
        //UserEntity user = em.find(UserEntity.class, id);
    
        em.merge(u);
        
        em.getTransaction().commit();
        System.out.println("Etape 5");

        
       
        
    }
    
    public static List<User> listUsers() {
    	
    	EntityManager em = Connexion.connexion();

    	em.getTransaction().begin();

    	//TypedQuery<UserEntity> requete = em.createQuery("select u from user u",UserEntity.class);
		TypedQuery<User> requete = em.createQuery("select u from user u",User.class);
    	
    	List<User> users  = requete.getResultList();
    	
    	em.getTransaction().commit();

        return users;
    }
    
    
    
public static User findUser(int id){
    	
    	EntityManager em = Connexion.connexion();
    	
    	
    
    	System.out.println(" dans find User");	
    	
    	  return em.find(User.class, id);

    	
       
    }


public static User findUserByOject(Object u){
	
	EntityManager em = Connexion.connexion();
	
	return em.find(User.class, u);

	
}
    
    
    
    public static void deleteUser(User u) {
    	EntityManager em = Connexion.connexion();
    	
    	
    	//Transaction 1 je crée mon objet
    	em.getTransaction().begin();
    	
    	User user = em.find(User.class,u.getIdUser());
    	em.remove(user);

    	em.getTransaction().commit();
    	 
    	System.out.println("remove--------------------");

    }
}
